$("a.predictions").on('click', function () {
  var e = $(this);
  $('.modal-content').empty();
  $('.modal-content').append('<div class="row" style="padding:20px;"><div class="col-md-2 col-md-offset-5"><img src="img/ajax-loader.gif" /></div></div>');
  $('#scores').removeData('bs.modal');
  $('#scores').on('loaded.bs.modal',function(e){
    $('a.country').tooltip();
  });
  $('#scores').modal({ remote: e.data('remote') });
  $('#scores').modal('show');
});

$(document).ready(function(e){
  $('a.country').tooltip();
});

