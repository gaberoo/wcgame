# WorldCup 2014 Brazil Betting Game

## Installation and usage

Enter the `wcgame` directory. For example:

```python
cd wcgame
```
Install the __python-excel__ package. You'll have to enter your password.

```python
cd xlrd
sudo python setup.py install
```

Put all entry files into the `entries` directory.

Enter the real scores in `Scores.xls`.

Calculate the points of all the players:

```python
python wcgame.py
```

