#!/usr/bin/env ruby

require 'sinatra'
require 'chartkick'
require 'json'
require 'date'

def games
  games = JSON.parse(`./wcgame.py games`)
  games.map do |game|
    regex = /\w, (?<month>\w+) (?<day>\d+)(st|nd|rd|th)/
    day = regex.match(game[2]) { |x| "#{x[:day]} #{x[:month]}" }
    time = game[3] > 0 ? (game[3]*24).to_i : 24
    timestamp = DateTime.parse("#{day} #{time}:00:00 +02:00")
    {
      id: game[0],
      round: game[1],
      team1: game[4],
      team2: game[5],
      time: timestamp
    }
  end
end

def cum_scores(in_scores = nil)
  scores = in_scores || JSON.parse(`./wcgame.py`)
  games = JSON.parse(File.open("public/games.json").read)
  times = games.map{ |x| DateTime.parse(x["time"]).to_time.to_i }
  scores.map do |name,score|
    times, score = times.zip(score).map!{ |x| x[1].nil? ? nil : x }.compact.sort.transpose
    cs = score.inject([0]) { |x,y| x + [x.last + y] }
    cs.shift
    #times = games.map{ |x| DateTime.parse(x["time"]).strftime("%d-%m-%Y %H:00:00 CET") }
    { name: name, data: times.zip(cs) }
  end
end

def sweepstake
  File.open("Sweepstake.txt").read.lines.map do |l|
    l = l.strip.split(/,/)
    { l[1] => l[0] }
  end.reduce :merge
end

helpers do
  def winner(x)
    if x[0].to_i > x[1].to_i
      return 1
    elsif x[1].to_i > x[0].to_i
      return -1
    else
      return 0
    end
  end
end

get '/' do
  erb :head, locals: { active: "home" } do
    erb :index
  end
end

get '/scores' do
  scores = JSON.parse(`./wcgame.py`)
  total_score = scores.map do |k,v|
    [ k , v.inject(:+) ]
  end.sort{ |a,b| b[1] <=> a[1] }
  erb :head, locals: { active: "scores" } do
    erb :scores, locals: { scores: total_score, cum_scores: JSON.generate(cum_scores(scores)) }
  end
end

get '/cum_scores' do
  JSON.pretty_generate(cum_scores)
end

get '/predictions/:name' do
  games = JSON.parse(File.open("public/games.json").read)
  pred = JSON.parse(`./wcgame.py predictions #{params[:name]}`).map do |x|
    { x[0] => [ x[1], x[2] ] }
  end.reduce :merge
  pred = games.map do |game|
    key = "#{game["team1"]} -- #{game["team2"]}"
    [key, pred[game["id"]]] if pred[game["id"]] 
  end.compact
  erb :predictions, locals: { name: params[:name], pred: pred, sweep: sweepstake }
end

get '/groups' do
  games = JSON.parse(File.open("public/games.json").read)
  now = DateTime.now
  upcoming = games.select{ |v| DateTime.parse(v["time"]) > now }
  upcoming.sort!{ |a,b| DateTime.parse(a["time"]) <=> DateTime.parse(b["time"]) }
  groups = JSON.parse(`./wcgame.py groups`)
  sw = sweepstake
  rounds = (1..5).to_a.map do |r|
    games.select{ |game| game["round"] == r }
  end
  i = 65
  ghash = groups.map do |g|
    i += 1
    { (i-1).chr => g }
  end.reduce :merge
  erb :head, locals: { active: "groups" } do
    erb :groups, locals: { 
      ghash: ghash, 
      countries: sw, 
      games: upcoming,
      rounds: rounds
    }
  end
end

#graph "Scores", :prefix => '/graphs' do
#  scores = JSON.parse(`./wcgame.py`).sort { |a,b| b[1] <=> a[1] }
#  scores.each do |name,score|
#    score = score.inject([0]) { |x,y| x + [x.last + y] }
#    score.shift
#    line name, score
#  end
#end
