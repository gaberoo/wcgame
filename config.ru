require 'sinatra'

set :env, :production
disable :run

require './web.rb'

run Sinatra::Application


