#!/usr/bin/env python3.4

import xlrd
import os
import sys
import json

# top-left field of scores (round robin)
g0 = [ 4, 38 ]

rounds = [
  [ 6,  56 ], # 0 round robin
  [ 58, 66 ], # 1 r16
  [ 68, 72 ], # 2 quarters
  [ 74, 76 ], # 3 semis
  [ 78, 79 ], # 4 small final
  [ 82, 83 ]  # 5 final
]

awards = dict()
for i in range(1, 49):
  awards[i] = [1, 3]
for i in range(49, 57):
  awards[i] = [2, 6]
for i in range(57, 61):
  awards[i] = [2, 6]
for i in range(61, 64):
  awards[i] = [4, 12]
awards[64] = [8, 24]

dirs = [
  "entries",
  "r16",
  "quarters",
  "semis",
  "final_third",
  "final"
]

# FUNCTIONS

##############################################################################

def get_scores(filename, r0 = 0, r1 = 6):
  "Get scores from an Excel file"
  scores = dict()
  book = xlrd.open_workbook(filename)
  sh = book.sheet_by_index(0)
  for r in range(r0, r1):
    for i in range(rounds[r][0], rounds[r][1]):
      game_id = sh.cell_value(i, 1)
      if game_id != "":
        x_name = sh.cell_value(i, 5)
        y_name = sh.cell_value(i, 8)
        x = sh.cell_value(i, 6)
        y = sh.cell_value(i, 7)
        #if x_name and y_name:
        #game_name = "%s -- %s" % (x_name, y_name)
        if x != "" and y != "":
          scores[int(game_id)] = [x, y]
  return scores

##############################################################################

def winner(x):
  if int(x[0]) > int(x[1]):
    return 1
  elif int(x[0]) < int(x[1]):
    return -1
  else:
    return 0

def is_number(s):
  try:
    float(s)
    return True
  except ValueError:
    return False

def penalty(x,p):
  y = [ x[0] - int(x[0]), x[1] - int(x[1]) ]
  if sum(y) > 0:
    z = [ p[0] - int(p[0]), p[1] - int(p[1]) ]
    if y == z:
      return 1
  return 0

##############################################################################

def calc_points(real, predicted):
  "Calculate points for a prediction"
  score = []
  for _key in real:
    value = real[_key]
    p = predicted[_key]
    if value[0] != "" and value[1] != "":
      s = 0
      if int(p[0]) == int(value[0]) and int(p[1]) == int(value[1]):
        s = awards[_key][1]
      elif winner(p) == winner(value):
        s = awards[_key][0]
      else:
        s = 0
      score.append(s + penalty(value,p))
  return score

##############################################################################

def calc_pred(real, predicted):
  "Get real and predictions"
  pred = []
  for _key in real:
    value = real[_key]
    p = predicted[_key]
    if value[0] != "" and value[1] != "":
      pred.append([_key, value, p])
  return pred

##############################################################################

def get_real():
  "Read real scores"
  return get_scores('Scores.xls',0)

##############################################################################

# Get groups
def get_groups():
  "Extract groups from excel sheet"
  groups = []
  book = xlrd.open_workbook('Scores.xls')
  sh = book.sheet_by_index(0)
  for i in range(0, 8):
    gr = []
    j0 = g0[0]+i*7
    for j in range(1, 5):
      name = sh.cell_value(j0+j, g0[1])
      points = sh.cell_value(j0+j, g0[1]+8)
      gr.append([name, points])
    groups.append(gr)
  return groups

##############################################################################

# Get games
def get_games(r0 = 0):
  "Get list of games"
  games = []
  book = xlrd.open_workbook('Scores.xls')
  sh = book.sheet_by_index(0)
  for r in range(r0, 6):
    for i in range(rounds[r][0], rounds[r][1]):
      num = sh.cell_value(i, 1)
      if num != "":
        date = sh.cell_value(i, 2)
        time = sh.cell_value(i, 3)
        team1 = sh.cell_value(i, 5)
        team2 = sh.cell_value(i, 8)
        group = sh.cell_value(i, 11)
        venue = sh.cell_value(i, 10)
        if team1 != "" and team2 != "":
          games.append([ int(num), r, date, time, team1, team2, group, venue ])
  return games

#print "The number of worksheets is", book.nsheets
#print "Worksheet name(s):", book.sheet_names()
#print sh.name, sh.nrows, sh.ncols
#print "Cell D30 is", sh.cell_value(rowx=29, colx=3)

# get entry files ############################################################

def get_entries():
  "Get all main entry files"
  r = 0
  entry_files = os.listdir(dirs[r])
  entries = dict()
  for entry in entry_files:
    name = os.path.splitext(entry)[0]
    if name[0] != '.':
      filename = dirs[r] + "/" + entry
      entries[name] = get_scores(filename,0)
  return entries

def get_updated_entry(entry, r = 0):
  "Try to get updated entries"
  filename = dirs[r] + "/" + entry + ".xls"
  if os.path.isfile(filename):
    return get_scores(filename,r)
  return dict()

def update_entry(key,entry):
  for r in range(1, 6):
    update = get_updated_entry(key, r)
    for game_id in update:
      entry[game_id] = update[game_id]
  return entry

# load partipants ############################################################

if __name__ == "__main__":
  run = "points"
  if len(sys.argv) > 1:
    run = sys.argv[1]

  # get entries
  entries = get_entries()

  if run == "points":
    points = dict()
    real_scores = get_real()
    for key in entries.keys():
      entries[key] = update_entry(key, entries[key])
      points[key] = calc_points(real_scores, entries[key])
    print(json.dumps(points))

  elif run == "games":
    print(json.dumps(get_games()))

  elif run == "real":
    print(json.dumps(get_real()))

  elif run == "groups":
    print(json.dumps(get_groups()))

  elif run == "predictions":
    real = get_real()
    entry = get_scores("entries/" + sys.argv[2] + ".xls")
    entry = update_entry(sys.argv[2],entry)
    print(json.dumps(calc_pred(real, entry)))

  

